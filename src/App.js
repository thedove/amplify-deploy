import logo from './logo.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Conngrat</p>
        <a
          className="App-link"
          href="https://eazytraining.fr/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn More on eazytraining
        </a>
      </header>
    </div>
  );
}

export default App;
